# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class SimpleHTTPServerFrame
###########################################################################

class SimpleHTTPServerFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 500,300 ), wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer6 = wx.BoxSizer( wx.VERTICAL )

		bSizer101 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer91 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText4 = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Serving Directory", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )

		bSizer91.Add( self.m_staticText4, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.dir_picker_txt = wx.DirPickerCtrl( self.m_panel1, wx.ID_ANY, wx.EmptyString, u"Select a folder", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_DEFAULT_STYLE )
		bSizer91.Add( self.dir_picker_txt, 1, 0, 5 )


		bSizer101.Add( bSizer91, 1, wx.EXPAND|wx.ALL, 5 )

		bSizer11 = wx.BoxSizer( wx.VERTICAL )

		self.update_path_btn = wx.Button( self.m_panel1, wx.ID_ANY, u"Update", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.update_path_btn.Hide()

		bSizer11.Add( self.update_path_btn, 0, wx.ALL, 5 )


		bSizer101.Add( bSizer11, 0, 0, 5 )


		bSizer6.Add( bSizer101, 0, wx.EXPAND, 5 )

		bSizer2 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer4 = wx.BoxSizer( wx.HORIZONTAL )

		self.port_name = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Serving on Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.port_name.Wrap( -1 )

		bSizer4.Add( self.port_name, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.port_txt = wx.TextCtrl( self.m_panel1, wx.ID_ANY, u"8000", wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_ENTER )
		self.port_txt.SetToolTip( u"Enter a port between 1-65535, or 0 for automatic port selection." )

		bSizer4.Add( self.port_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.auto_port_chk = wx.CheckBox( self.m_panel1, wx.ID_ANY, u"Auto Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.auto_port_chk.Hide()

		bSizer4.Add( self.auto_port_chk, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer2.Add( bSizer4, 1, wx.ALL, 5 )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		self.start_btn = wx.Button( self.m_panel1, wx.ID_ANY, u"Start", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.start_btn, 0, wx.ALL, 5 )


		bSizer2.Add( bSizer3, 0, wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer6.Add( bSizer2, 0, wx.EXPAND, 5 )

		self.m_staticline1 = wx.StaticLine( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer6.Add( self.m_staticline1, 0, wx.EXPAND |wx.ALL, 5 )

		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Logs" ), wx.VERTICAL )

		self.logs_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_WORDWRAP )
		sbSizer1.Add( self.logs_txt, 1, wx.EXPAND, 5 )


		bSizer6.Add( sbSizer1, 1, wx.EXPAND|wx.ALL, 5 )

		bSizer8 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

		self.browse_txt = wx.StaticText( self.m_panel1, wx.ID_ANY, u"Test In browser", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.browse_txt.Wrap( -1 )

		self.browse_txt.Hide()

		bSizer10.Add( self.browse_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		url_choice_cmbChoices = []
		self.url_choice_cmb = wx.Choice( self.m_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, url_choice_cmbChoices, 0 )
		self.url_choice_cmb.SetSelection( 0 )
		self.url_choice_cmb.Hide()
		self.url_choice_cmb.SetToolTip( u"Select an IP to test" )

		bSizer10.Add( self.url_choice_cmb, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer10.Add( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer8.Add( bSizer10, 1, wx.EXPAND, 5 )

		bSizer9 = wx.BoxSizer( wx.VERTICAL )

		self.clear_log_btn = wx.Button( self.m_panel1, wx.ID_ANY, u"Clear Log", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer9.Add( self.clear_log_btn, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer9, 0, wx.EXPAND, 5 )


		bSizer6.Add( bSizer8, 0, wx.EXPAND|wx.ALL, 5 )


		self.m_panel1.SetSizer( bSizer6 )
		self.m_panel1.Layout()
		bSizer6.Fit( self.m_panel1 )
		bSizer1.Add( self.m_panel1, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem1 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem1 )

		self.m_menubar1.Append( self.m_menu1, u"File" )

		self.m_menu2 = wx.Menu()
		self.m_menuItem2 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"Beer", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.m_menuItem2 )

		self.m_menuItem3 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.m_menuItem3 )

		self.m_menubar1.Append( self.m_menu2, u"Help" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.on_close )
		self.dir_picker_txt.Bind( wx.EVT_DIRPICKER_CHANGED, self.on_local_path )
		self.update_path_btn.Bind( wx.EVT_BUTTON, self.on_update_path )
		self.port_txt.Bind( wx.EVT_TEXT, self.on_port_update )
		self.port_txt.Bind( wx.EVT_TEXT_ENTER, self.on_start )
		self.start_btn.Bind( wx.EVT_BUTTON, self.on_start )
		self.url_choice_cmb.Bind( wx.EVT_CHOICE, self.on_browse )
		self.clear_log_btn.Bind( wx.EVT_BUTTON, self.on_clear_log )
		self.Bind( wx.EVT_MENU, self.on_exit, id = self.m_menuItem1.GetId() )
		self.Bind( wx.EVT_MENU, self.on_beer, id = self.m_menuItem2.GetId() )
		self.Bind( wx.EVT_MENU, self.on_about_box, id = self.m_menuItem3.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_close( self, event ):
		event.Skip()

	def on_local_path( self, event ):
		event.Skip()

	def on_update_path( self, event ):
		event.Skip()

	def on_port_update( self, event ):
		event.Skip()

	def on_start( self, event ):
		event.Skip()


	def on_browse( self, event ):
		event.Skip()

	def on_clear_log( self, event ):
		event.Skip()

	def on_exit( self, event ):
		event.Skip()

	def on_beer( self, event ):
		event.Skip()

	def on_about_box( self, event ):
		event.Skip()


