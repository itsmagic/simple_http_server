import wx
import wx.adv
import os
import queue
import sys
import simple_http_server_gui
import webbrowser
from pydispatch import dispatcher
from scripts import auto_update, http_server, interface_monitor

# The MIT License (MIT)

# Copyright (c) 2021 Jim Maciejewski

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


class Simple_HTTP_Server(simple_http_server_gui.SimpleHTTPServerFrame):
    def __init__(self, parent):
        simple_http_server_gui.SimpleHTTPServerFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "wu.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "Simple HTTP Server"
        self.version = "v0.1.3"
        self.local_path = os.getcwd() # The path we are in
        self.SetTitle(f'{self.name} {self.version}')
        self.check_for_updates = True

        dispatcher.connect(self.server_status, signal="Server Status")
        dispatcher.connect(self.update_required, signal="Software Update")
        dispatcher.connect(self.interface_update, signal="Interface Update")

        if self.check_for_updates:
            update_thread = auto_update.AutoUpdate(server_url="https://magicsoftware.ornear.com", program_name=self.name, program_version=self.version)
            update_thread.setDaemon(True)
            update_thread.start()

        self.ip_address_list = []
        self.interface_queue = queue.Queue()
        ip_monitor = interface_monitor.InterfaceMonitor(self.interface_queue)
        ip_monitor.setDaemon(True)
        ip_monitor.start()
        self.interface_queue.put(['send_update'])

        self.http_server_thread = http_server.MyHTTPServer()
        self.http_server_thread.setDaemon(True)
        self.http_server_thread.start()

        self.on_port_update(None)
        self.dir_picker_txt.SetPath(self.local_path)

        self.refresh_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_refresh, self.refresh_timer)
        self.refresh_timer.Start(1000)

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def update_required(self, sender, message, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)

    def on_refresh(self, event):
        self.interface_queue.put(['check_changes'])

    def interface_update(self, interface_list):
        self.ip_address_list = []
        for interface in interface_list:
            self.ip_address_list.append(interface.ip_addresses[0])



    def server_status(self, data):
        if "server" in data:
            if data['server'] == "stopped":
                self.server_stopped()
                self.url_choice_cmb.Hide()
                self.browse_txt.Hide()
        if "port" in data:

            self.url_choice_cmb.Clear()
            if len(self.ip_address_list) > 0:
                for ip_address in self.ip_address_list:
                    self.url_choice_cmb.Append(f"http://{ip_address}:{data['port']}")
            else:
                self.url_choice_cmb.Append(f"http://localhost:{data['port']}")
            self.url_choice_cmb.SetSelection(0)
            self.url_choice_cmb.Show()
            self.browse_txt.Show()
            self.Layout()
        if "log" in data:
            self.logs_txt.AppendText(f"{data['log']}\n")
        if "error" in data:
            dlg = wx.MessageDialog(parent=self,
                                   message=f"Sorry that port is in use, or you don't have permission to use it.\rYou can try port 0 for auto port selection.\r\r{data['message']}",
                                   caption='Port unavailable',
                                   style=wx.OK)

            dlg.ShowModal()
            self.stop_server()

    def on_browse(self, event):
        url = self.url_choice_cmb.GetStringSelection()
        if url != '':
            webbrowser.open_new_tab(url)



    def on_local_path(self, event):
        self.local_path = self.dir_picker_txt.GetPath()


    def on_port_update(self, event):
        port_number = self.port_txt.GetValue()
        if port_number.isdigit() and int(port_number) >= 0 and int(port_number) <= 65535:
            # Valid port
            self.start_btn.Enable()
            self.port_txt.SetBackgroundColour(wx.NullColour)
            self.port_txt.Refresh()
        else:
            # Invalid port
            self.start_btn.Disable()
            self.port_txt.SetBackgroundColour('red')
            self.port_txt.Refresh()

    def on_clear_log(self, event):
        self.logs_txt.Clear()

    def on_start(self, event):
        if self.start_btn.GetLabel() == "Start":
            self.start_btn.SetLabel("Stop")
            self.start_server()
        else:
            self.stop_server()
            

    def start_server(self):
        # Hide port controls
        self.port_txt.Disable()
        self.dir_picker_txt.Disable()
        
        # Clear out log
        self.on_clear_log(None)
        
        # Start HTTP server
        port = int(self.port_txt.GetValue())
        dispatcher.send(signal="Server Control", data={"server": "start", "port": port, "directory": self.local_path})

    def stop_server(self):
        # Stop server
        dispatcher.send(signal="Server Control", data={"server": "stop"})

    def server_stopped(self):
        # Show port controls
        self.port_txt.Enable()
        self.dir_picker_txt.Enable()
        self.start_btn.SetLabel("Start")

    def on_close(self, event):
        self.Hide()
        dispatcher.send(signal="Shutdown")
        event.Skip()

    def on_exit(self, event):
        self.Close()

    def on_about_box(self, event):
        """Show the About information"""

        description = """The Simple HTTP Server is a tool that allow you to host
files in a folder as if they were on a webserver.
"""

        licence = """The MIT License (MIT)

Copyright (c) 2021 Jim Maciejewski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

        info = wx.adv.AboutDialogInfo()
        info.SetName(self.name)
        info.SetVersion(self.version)
        info.SetDescription(description)
        info.SetLicence(licence)
        info.AddDeveloper('Jim Maciejewski')
        wx.adv.AboutBox(info)

    def on_beer(self, event):
        """ Buy me a beer! Yea!"""
        dlg = wx.MessageDialog(parent=self, message='If you enjoy this program \n Learn how you can help out',
                               caption='Buy me a beer',
                               style=wx.OK)
        if dlg.ShowModal() == wx.ID_OK:
            url = 'http://ornear.com/give_a_beer'
            webbrowser.open_new_tab(url)
        dlg.Destroy()



def main():
    """run the main program"""
    ss_app = wx.App()  # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    ss_frame = Simple_HTTP_Server(None)
    ss_frame.Show()
    ss_app.MainLoop()


if __name__ == '__main__':
    main()
