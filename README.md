# Simple HTTP Server -- _a_ _way_ _to_ _quickly_ _host_ _files_

## Intent
There are times when you need to have files available via a HTTP webserver, however running a production webserver is impractical. 

### Why
1. Should run on any Windows machine 
2. Upgrading firmware sometimes requires a HTTP server
3. Troubleshooting HTTP requests
4. Sharing files on the local network

### Concepts
The Simple HTTP Server is a graphical frontend to the Python [http.server](https://docs.python.org/3/library/http.server.html) 

Leveraging the ThreadingHTTPServer, Simple HTTP Server allows multiple connections, however it is not recommended for production. It only implements basic security checks.

It allows easy configuration of the listening port, starting and stopping of the HTTP server and logging of the requests. 

It will attempt to bind any port between 1-65535, or set the port to 0 for automatic port selection. 

Once the server is started, you should be able to browse to the server locally using "localhost" or remotely using your network IP address. Your Windows firewall will need to allow the inbound connection. 

### Latest Windows Executable

Download the latest [version](https://magicsoftware.ornear.com/updates/simple_http_server/) 