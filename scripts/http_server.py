from http import server
from threading import Thread
import os
import sys
import time
from pydispatch import dispatcher
from http.server import HTTPServer, SimpleHTTPRequestHandler
from socketserver import ThreadingMixIn


class SimpleHTTPRequestHandler(SimpleHTTPRequestHandler):

    def log_message(self, format, *args):
        dispatcher.send(signal="Server Status", data={"log": ("%s - - [%s] %s" %
                         (self.address_string(),
                          self.log_date_time_string(),
                          format%args))})
  
  
class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
    pass


class MyHTTPThread(Thread):
    def __init__(self, port, directory):
        self.port = port
        self.directory = directory
        self.server = None
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        Thread.__init__(self)

    def shutdown_signal(self):
        if self.server is not None:
            self.server.shutdown()

    def run(self):
        # print("running")
        try:
            os.chdir(self.directory)
            self.server = ThreadingSimpleServer(('', self.port), SimpleHTTPRequestHandler)
            self.server.block_on_close = False
        except (OSError, OverflowError) as error:
            dispatcher.send(signal="Server Status", data={"error": "port unavailable", "message": error})
            dispatcher.send(signal="Server Status", data={"server": f"stopped"})
            return


        current_server_port = self.server.socket.getsockname()[1]
        dispatcher.send(signal="Server Status", data={"log": f"Server started on port {current_server_port} ...."})
        # dispatcher.send(signal="Server Status", data={"log": f"http://<your ip address>:{current_server_port}/<your file>"})
        dispatcher.send(signal="Server Status", data={"port": f"{current_server_port}"})
        self.server.serve_forever(poll_interval=1)
        dispatcher.send(signal="Server Status", data={"log": f"Server stopping...."})
        self.server.server_close()
        dispatcher.send(signal="Server Status", data={"log": f"Server stopped, no new requests will be served."})
        dispatcher.send(signal="Server Status", data={"server": f"stopped"})


class MyHTTPServer(Thread):
    def __init__(self):
        self.myserver = None
        self.shutdown_now = False
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        dispatcher.connect(self.control,
                           signal="Server Control",
                           sender=dispatcher.Any)
        Thread.__init__(self)

    def shutdown_signal(self):
        self.shutdown_now = True
        if self.myserver is not None:
            self.myserver.server.shutdown()
            self.myserver = None

    def run(self):
        while not self.shutdown_now:
            time.sleep(1)

    def control(self, data):
        if "server" in data:
            if data['server'] == 'start':
                self.start_server(port=data['port'], directory=data['directory'])
            if data['server'] == 'stop':
                # print("sending shutdown")
                if self.myserver.server is not None:
                    self.myserver.server.shutdown()
                self.myserver = None

    def start_server(self, port, directory):
        self.myserver = MyHTTPThread(port, directory)
        self.myserver.setDaemon(True)
        self.myserver.start()

def message(data):
    print(data)

if __name__ == '__main__':
    dispatcher.connect(message, signal="Server Status")
    server_port = 8000
    serv = MyHTTPServer()
    serv.daemon = True
    serv.start()
    import time
    dispatcher.send(signal="Server Control", data={"server": "start", "port": 8000})
    print("waiting")
    time.sleep(10)
    dispatcher.send(signal="Server Control", data={"server": "stop"})
    print("Done")
    dispatcher.send(signal="Server Control", data={"server": "start", "port": 8000})
    print("waiting")
    time.sleep(10)
    dispatcher.send(signal="Server Control", data={"server": "stop"})